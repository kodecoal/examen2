import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegistroComponent } from './registro/registro.component';
import { ArtistasComponent } from './artistas/artistas.component';
import { ListadoComentariosComponent } from './listado-comentarios/listado-comentarios.component';
import { DetalleComentarioComponent } from './detalle-comentario/detalle-comentario.component';


const routes: Routes = [
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'registro',
    component:RegistroComponent
  },
  {
    path:'artistas',
    component:ArtistasComponent
  },
  {
    path:'listado',
    component:ListadoComentariosComponent
  },
  {
    path:'detalle',
    component:DetalleComentarioComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
