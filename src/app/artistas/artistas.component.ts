import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-artistas',
  templateUrl: './artistas.component.html',
  styleUrls: ['./artistas.component.css']
})
export class ArtistasComponent implements OnInit {
  constructor() { }

  art={
    nombre:null,
    descripcion:null,
    categoria:null
  }

  articulos = [{nombre:'The Prodigy', descripcion:'Banda de HardTechno de londres', categoria:'Techno'},
               {nombre:'Daft Punk', descripcion:'Banda de house', categoria:'Techno'},
               {nombre:'The Chemical Brothers', descripcion:'Banda de house', categoria:'Techno'},
               {nombre:'Underworld', descripcion:'Banda de house', categoria:'Techno'},
               {nombre:'Scooter', descripcion:'Banda de house', categoria:'Techno'},
               {nombre:'Orbital', descripcion:'Banda de house', categoria:'Techno'},

              ];

  hayRegistros() {
    return this.articulos.length>0;
  }

  borrar(art) {
    for(let x=0;x<this.articulos.length;x++)
      if (this.articulos[x].nombre==art.nombre)
      {
        this.articulos.splice(x,1);
        return;
      }
  }

  agregar() {
    for(let x=0;x<this.articulos.length;x++)
    if (this.articulos[x].nombre==this.art.nombre)
    {
      alert('ya existe un articulo con dicho nombre');
      return;
    }
    this.articulos.push({nombre:this.art.nombre,
                         descripcion:this.art.descripcion,
                         categoria:this.art.categoria });
    this.art.nombre=null;
    this.art.descripcion=null;
    this.art.categoria=null;
  }

  seleccionar(art) {
    this.art.nombre=art.nombre;
    this.art.descripcion=art.descripcion;
    this.art.categoria=art.categoria;
  }
  ngOnInit() {
  }
}
