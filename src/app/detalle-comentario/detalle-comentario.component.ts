import { Component, OnInit, Input } from '@angular/core';
import { Comentario } from '../model/Comentario';

@Component({
  selector: 'app-detalle-comentario',
  templateUrl: './detalle-comentario.component.html',
  styleUrls: ['./detalle-comentario.component.css']
})
export class DetalleComentarioComponent implements OnInit {
  @Input() mySelectedComentario: Comentario;

  constructor() { }

  ngOnInit() {
  }

}
