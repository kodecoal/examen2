import { Component, OnInit } from '@angular/core';
import { Comentario } from '../model/Comentario';

@Component({
  selector: 'app-listado-comentarios',
  templateUrl: './listado-comentarios.component.html',
  styleUrls: ['./listado-comentarios.component.css']
})
export class ListadoComentariosComponent implements OnInit {
  misComentarios:Array<Comentario>;
  miComentario: Comentario;

  constructor() {
    this.misComentarios = new Array();
    this.miComentario = new Comentario();

    const comentario1 = {
      id: 1,
      texto: 'Esta Banda me encanta',
      autor: 'Jose Fernandez'
      };
      const comentario2 = {
      id: 2,
      texto: 'Muy adelantados a su epoca',
      autor: 'Pedro Perez'
      };

      this.misComentarios.push(comentario1);
      this.misComentarios.push(comentario2);
         }

  ngOnInit() {
      }
      addNewComentario(){
        this.misComentarios.push(this.miComentario)
        this.miComentario = new Comentario();
       }
}
