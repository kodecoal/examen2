import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { NgModule } from '@angular/core';

//Mis Material
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';

@NgModule({
  imports: [
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    FormsModule,
    MatCheckboxModule,
    MatInputModule,
    ],
  exports: [
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    FormsModule,
    MatCheckboxModule,
    MatInputModule,
    ],
})
export class MyOwnCustomMaterialModule { }
